package Tree;

import java.util.Stack;

public class Tree {

    private Element mainElement;

    public void add(Element element) {

        Element current;
        if (mainElement == null) {

            mainElement = element;

            System.out.println(mainElement);
        } else {
            current = mainElement;

            do {

                if (element.getValue() > current.getValue()) {

                    //current = current.greaterElement;
                    if (current.greaterElement == null) {
                        current.greaterElement = element;
                        current.greaterElement.previous = current;
                        System.out.println(current.greaterElement);
                        return;
                    } else {
                        current = current.greaterElement;

                    }
                }
                if (element.getValue() < current.getValue()) {

                    //current = current.greaterElement;
                    if (current.lowerElement == null) {
                        current.lowerElement = element;

                        current.lowerElement.previous = current;
                        System.out.println(current.lowerElement);
                        return;
                    } else {
                        current = current.lowerElement;

                    }
                }
                if (element.getValue() == current.getValue()) {
                    System.out.println("element " + element + " Already exist!");
                    return;
                }

            } while (true);
        }


    }

//    public void showLeft() {
//        //String str = " ";
//        int n = 30;
//        Element current = mainElement;
//        Element previous = mainElement;
//        whiteSpaces(n);
//        System.out.println(mainElement);
//        boolean lowerChecked = false;
//        do {
//
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && !lowerChecked) {
//                n -= 5;
//                previous = current;
//                current = current.lowerElement;
//                whiteSpaces(n);
//                System.out.println(current);
//            }
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && lowerChecked) {
//
//                previous = current;
//                current = current.greaterElement;
//                n += 2;
//                whiteSpaces(n);
//                System.out.println(current);
//
//            }
//            if (current.greaterElement != null
//                    && current.lowerElement == null) {
//                n += 2;
//                current = current.greaterElement;
//                whiteSpaces(n);
//                System.out.println(current);
//            }
//            if (current.greaterElement == null
//                    && current.lowerElement != null) {
//
//                current = current.lowerElement;
//                n -= 5;
//                whiteSpaces(n);
//                System.out.println(current);
//            }
//            if (current.greaterElement == null
//                    && current.lowerElement == null
//                    && !lowerChecked) {
//
//                current = previous;
//                lowerChecked = true;
//            }
//
//            if (current.greaterElement == null
//                    && current.lowerElement == null
//                    && lowerChecked
//                    ) {
//                return;
//            }
//
//
//        } while (true);
//    }

//    public void showRight() {
//        int n = 30;
//        Element current = mainElement;
//        Element previous = mainElement;
//        // System.out.println(mainElement);
//
//        boolean rightChecked = false;
//        do {
//
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && !rightChecked) {
//
//                previous = current;
//                n += 5;
//                current = current.greaterElement;
//                //               whiteSpaces(n);
//                System.out.println(current);
//            }
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && rightChecked) {
//
//                previous = current;
//                current = current.lowerElement;
//                n -= 5;
//                //              whiteSpaces(n);
//                System.out.println(current);
//
//            }
//            if (current.greaterElement != null
//                    && current.lowerElement == null) {
//
//                current = current.greaterElement;
//                n += 5;
//                //             whiteSpaces(n);
//                System.out.println(current);
//            }
//            if (current.greaterElement == null
//                    && current.lowerElement != null) {
//
//                current = current.lowerElement;
//                n -= 5;
//                //       whiteSpaces(n);
//                System.out.println(current);
//            }
//            if (current.greaterElement == null
//                    && current.lowerElement == null
//                    && !rightChecked) {
//
//                current = previous;
//                rightChecked = true;
//            }
//
//
//            if (current.greaterElement == null
//                    && current.lowerElement == null
//                    && rightChecked
//                    ) {
//                return;
//            }
//        } while (true);
//    }

    public void whiteSpaces(int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(" ");
        }
    }

    //    public void remove(Element element){
//        Element current = mainElement;
//        if (current)
//    }
//    public void show2() {
//        boolean leftSideChecked = false;
//        boolean rightSide = false;
//        Element current = mainElement;
//        do {
//
//
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && !leftSideChecked) {
//
//                current = current.lowerElement;
//                System.out.println(current);
//            }
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && leftSideChecked) {
//
//                current = current.greaterElement;
//                System.out.println(current);
//            }  if (current.lowerElement == null
//                    && current.greaterElement != null) {
//
//                current = current.greaterElement;
//                System.out.println(current);
//            }
//
//            if (current.lowerElement == null
//                    && current.greaterElement == null) {
//
//                current = current.previous;
//                leftSideChecked = true;
//                rightSide = true;
//            }
//            if (current.lowerElement != null
//                    && current.greaterElement != null
//                    && leftSideChecked
//                    && rightSide){
//                current = current.previous;
//                leftSideChecked = true;
//                rightSide = true;
//            }
//            if (current.greaterElement == null &&
//                    current.lowerElement == null
//                            && leftSideChecked){
//                return;
//            }
//        } while (true);
//    }

    public void display() {
        Stack globalStack = new Stack();
        globalStack.push(mainElement);
        int nBlanks = 32;
        boolean isRowEmpty = false;
        System.out.println("......................................................");

        while (isRowEmpty == false) {
            Stack localStack = new Stack();
            isRowEmpty = true;
            for (int i = 0; i < nBlanks; i++) {
                System.out.print(" ");
            }
            while (globalStack.isEmpty() == false) {
                Element temp = (Element) globalStack.pop();
                if (temp != null) {
                    System.out.print(temp);
                    localStack.push(temp.lowerElement);
                    localStack.push(temp.greaterElement);

                    if (temp.lowerElement != null
                            || temp.greaterElement != null) {
                        isRowEmpty = false;
                    }
                } else {
                    System.out.print("--");
                    localStack.push(null);
                    localStack.push(null);
                }
                for (int i = 0; i < nBlanks * 2 - 2; i++) {
                    System.out.print(" ");
                }
            }
            System.out.println();
            nBlanks /= 2;
            while (localStack.isEmpty() == false) {
                globalStack.push(localStack.pop());
            }

            System.out.println(".................................................................");
        }

    }

    public void remove(Element element) {
        Element current = mainElement;
        Element previous = null;
        do {

            if (element.getValue() > current.getValue()) {
                previous = current;
                current = current.greaterElement;
            } else if (element.getValue() < current.getValue()) {
                previous = current;
                current = current.lowerElement;
            } else if (current.greaterElement == null &&
                    current.lowerElement == null) {
                System.out.println("Element doesn't exist");
                return;
            }
        } while (current.getValue() != element.getValue());

        if (current.lowerElement == null
                && current.greaterElement == null) {
            if (current == mainElement) {
                mainElement = null;
            } else if (current.getValue() > previous.getValue()) {
                previous.greaterElement = null;
            } else if (current.getValue() < previous.getValue()) {
                previous.lowerElement = null;
            }
        } else if (current.lowerElement != null
                && current.greaterElement == null) {
            if (current == mainElement) {
                mainElement = current.lowerElement;
            }
            if (current.getValue() > previous.getValue()) {
                previous.greaterElement = current.lowerElement;
            } else if (current.getValue() < previous.getValue()) {
                previous.lowerElement = current.lowerElement;
            }
        } else if (current.lowerElement == null
                && current.greaterElement != null) {
            if (current == mainElement) {
                mainElement = current.greaterElement;
            } else if (current.getValue() > previous.getValue()) {
                previous.greaterElement = current.greaterElement;
            } else if (current.getValue() < previous.getValue()) {
                previous.lowerElement = current.greaterElement;
            }
        } else {
                Element successor = getSuccessor(current);
                if (current == mainElement){
                    mainElement = successor;
                }else if (current.getValue() > previous.getValue()){
                    previous.greaterElement = successor;
                    successor.lowerElement = current.lowerElement;
                }else {
                    previous.lowerElement = successor;
                    successor.lowerElement = current.lowerElement;
                }

        }
    }

    private Element getSuccessor(Element element) {
        Element successorParent = element;
        Element successor = element;
        Element current = element.greaterElement;

        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.lowerElement;
        }
        if (successor != element.greaterElement) {
            successorParent.lowerElement = successor.greaterElement;
            successor.greaterElement = element.greaterElement;
        }
        return successor;

    }
}

