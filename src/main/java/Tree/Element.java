package Tree;

public class Element {
    private int value;
    Element lowerElement;
    Element greaterElement;
    Element previous;

    public Element(int v){
        value = v;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return " "+getValue();
    }
}
