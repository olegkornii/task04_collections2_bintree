import Tree.Tree;
import Tree.Element;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();
        Element element = new Element(10);
        Element element11 = new Element(3);
        Element element1 = new Element(2);
        Element element2 = new Element(100);
        Element element3 = new Element(80);
        Element element4 = new Element(4);
        Element element5 = new Element(1);
        Element element6 = new Element(120);
        Element element7 = new Element(74);
        Element element8 = new Element(5);
        Element element9 = new Element(90);
        Element element10 = new Element(30);
        Element element12 = new Element(85);
        Element element13 = new Element(76);
        Element element14 = new Element(35);
        Element element15 = new Element(140);
        Element element16 = new Element(121);
        Element element17 = new Element(25);


        tree.add(element);
        tree.add(element1);
        tree.add(element2);
        tree.add(element3);
        tree.add(element4);
        tree.add(element5);
        tree.add(element6);
        tree.add(element7);
        tree.add(element8);
        tree.add(element9);
        tree.add(element10);
        tree.add(element11);
        tree.add(element12);
        tree.add(element13);
        tree.add(element14);
        tree.add(element15);
        tree.add(element16);
        tree.add(element17);
        tree.add(new Element(77));
        tree.add(new Element(75));
        String string = "_";


        //System.out.println(string);
    //    tree.showLeft();
        //tree.show2();
        tree.display();
        tree.remove(element10);
        tree.display();
    }
}
